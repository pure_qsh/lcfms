package cn.lcfms.bin.view;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;


public class CommonEditView extends ModelAndView{
	private String title;
	private HashSet<HashMap<String, Object>> form;
	private String action;
	public CommonEditView(String title){
		this.setViewName("admin/table/editpage");
		this.title=title;
		this.addObject("CommonEditView", this);
	}
	public String getTitle() {
		return title;
	}
	/**
	 * 
	 * @param title 标题
	 * @param name name属性
	 * @param value 默认值
	 * @param placeholder 提示
	 * @param validate 验证,多个用空格隔开
	 * unempty 非空
	 * gt整数  大于整数个字符  如:gt5
	 * lt整数 小于整数个字符   如:lt5
	 * number 数字
	 * integer 整数
	 * +integer 正整数
	 * unchinaChar 非中文
	 * unNum 非数字
	 * email 邮件
	 * mobile 手机号
	 * readonly 只读
	 */
	public void setInputForm(String title,String name,Object value,String validate){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("value", value);		
		if(validate.equals("readonly")) {
			map.put("readonly", validate);
		}else {
			map.put("validate", validate);
		}
		map.put("type", "input");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	public void setInputForm(String title,String name,Object value,String validate,String placeholder){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("value", value);
		if(validate.equals("readonly")) {
			map.put("readonly", validate);
		}else {
			map.put("validate", validate);
		}
		map.put("placeholder", placeholder);
		map.put("type", "input");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}	
	/**
	 * 设置多行文本类型
	 * @param title
	 * @param name
	 * @param value
	 * @param maxnum 最大字数
	 */
	public void setTextareaForm(String title,String name,Object value,int maxnum){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("value", value);
		map.put("maxnum", maxnum);
		map.put("type", "textarea");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置隐藏类型
	 * @param name
	 * @param value
	 */
	public void setHiddenForm(String name,Object value){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("name", name);
		map.put("value", value);
		map.put("type", "hidden");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置checkbox表单类型
	 * @param title
	 * @param name
	 * @param text
	 * @param value
	 * @param def
	 */
	public void setCheckboxForm(String title,String name,String text,String value,String def) {
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("text", text.split(","));
		map.put("value", value.split(","));
		String[] split = def.split(",");
		HashSet<String> set=new HashSet<>();
		for(int i=0;i<split.length;i++) {
			set.add(split[i]);
		}
		map.put("def", set);
		map.put("type", "checkbox");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置radio表单类型
	 * @param title
	 * @param name
	 * @param text
	 * @param value
	 * @param def
	 */
	public void setRadioForm(String title,String name,String text,String value,String def) {
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("text", text.split(","));
		map.put("value", value.split(","));
		map.put("def", def);
		map.put("type", "radio");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置富文本编辑器类型
	 * @param title
	 * @param name
	 * @param value
	 */
	public void setEditorForm(String title,String name,Object value){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("value", value);
		map.put("type", "editor");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置下拉框类型
	 * @param title
	 * @param name
	 * @param text
	 * @param value
	 */
	public void setSelectForm(String title,String name,String text,String value,Object def){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("text", text.split(","));
		map.put("value", value.split(","));
		map.put("def", String.valueOf(def));
		map.put("type", "select");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 自定义表单jsp
	 * @param jspFile
	 * 根目录是模板的admin
	 */
	public void setViewForm(String jspFile){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("type", "jspview");
		map.put("file", jspFile);
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置地理城市选择类型
	 * @param title 标题
	 * @param level 级数，一级只显示省份,二级显示省份跟地区,三级显示县和区
	 */
	public void setAreasForm(String title,int level){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("level", level);
		map.put("type", "areas");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置地理城市选择类型
	 * @param title 标题
	 * @param level 级数，一级只显示省份,二级显示省份跟地区,三级显示县和区
	 * @param defaultValue 默认值，数组的长度必须与level保持一致
	 */
	public void setAreasForm(String title,int level,int[] defaultValue){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("level", level);
		map.put("type", "areas");
		int[] d=new int[4];
		for(int i=0;i<defaultValue.length;i++) {
			d[i]=defaultValue[i];
		}
		map.put("defaultValue", d);
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置上传图片文件,上传成功的文件将保存在js的form.upolodFileInfo这个属性里
	 * @param title
	 */
	public void setUploadImageForm(String title){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("type", "uploadImg");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	
	/**
	 * 设置上传文件的表单类型,上传成功的文件将保存在js的form.upolodFileInfo这个属性里
	 * @param title 
	 * @param fileType 文件类型,可以提交文件的扩展名
	 * @param fileSize 文件大小，单位是mb
	 * @param fileNum 允许上传的文件数目
	 */
	public void setUploadFileForm(String title,String fileType,int fileSize,int fileNum){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("type", "uploadFile");
		map.put("fileType", fileType);
		map.put("fileSize", fileSize*1024*1024);
		map.put("fileNum", fileNum);
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);		
	}
	
	/**
	 * 日期区间选择框
	 * @param title
	 * @param direction  有三种选择 future表示禁止过去,past表示禁止未来,""表示不禁止
	 */
	public void setDatesliceForm(String title,String direction,String begin,String end) {
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("type", "dateslice");
		map.put("begin", begin);
		map.put("end", end);
		if(!direction.equals("future")&&!direction.equals("past")&&!direction.equals("")) {
			map.put("direction", "future");
		}else {
			map.put("direction", direction);
		}		
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);	
	}
	/**
	 * 日期区间选择框
	 * @param title
	 */
	public void setDatesliceForm(String title) {
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("type", "dateslice");	
		map.put("direction", "future");	
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
		Date date=new Date();
		map.put("begin", sdf.format(date));
		map.put("end", sdf.format(date));
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);	
	}
	/**
	 *  单个日期选择框
	 * @param title
	 * @param direction  有三种选择 future表示禁止过去,past表示禁止未来,""表示不禁止
	 */
	public void setDateForm(String title,String direction,String value) {
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("type", "date");
		map.put("value", value);
		if(!direction.equals("future")&&!direction.equals("past")&&!direction.equals("")) {
			map.put("direction", "future");
		}else {
			map.put("direction", direction);
		}		
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);	
	}
	/**
	 * 单个日期选择框
	 * @param title
	 */
	public void setDateForm(String title) {
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("type", "date");	
		map.put("direction", "future");	
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
		Date date=new Date();
		map.put("value", sdf.format(date));
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);	
	}
	/**
	 * 设置表单的action
	 * @param action
	 */
	public void setFormAction(String action){
		this.action=action;
	}
	
	@SuppressWarnings("unused")
	private void setEditPage(HttpServletRequest request){
		request.setAttribute("form", form);
		request.setAttribute("action", action);
		request.setAttribute("title", title);
	}
}
