package cn.lcfms.bin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


public interface BaseDao {
	@Select("${sql}")
	public List<HashMap<String,Object>> selectList(Map<?,?> map);
	
	@Delete("${sql}")
	public int delete(Map<?,?> map);
	
	@Update("${sql}")
	public int update(Map<?,?> map);
	
	@Insert("${sql}")
	public int insert(Map<?,?> map);	
	
	@Insert("${sql}")
	public int insertTest(@Param(value = "sql") String sql);

}
