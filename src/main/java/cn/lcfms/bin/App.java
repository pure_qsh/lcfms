package cn.lcfms.bin;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;



public class App implements ApplicationContextAware{
	private static Logger logger = Logger.getLogger(App.class); 
	private static SqlSessionFactory factory=null;
	private static DataSourceTransactionManager transactionManager=null;
	public static String ROOT_PATH="";//系统根目录
	public static String CONF_PATH="";//系统配置目录
	public static List<HashMap<String, Object>> ITEM;//导航列表
	public static HashMap<String, Object> APPCONFIG;
	public static HashMap<Long, int[]> BROADCAST_ID;//广播识别ID
	private final static String ROOTNAME="evan.laocheng";
    
    public void init(){
    	System.out.println("初始化系统");
    	//设置根目录
    	if(ROOT_PATH.equals("")){
    		ROOT_PATH = System.getProperty(ROOTNAME);
    		CONF_PATH = ROOT_PATH+"WEB-INF"+File.separator+"classes"+File.separator;
    	}
    	if(APPCONFIG==null){
    		APPCONFIG=new HashMap<String, Object>();
    		ResourceBundle config=ResourceBundle.getBundle("lcfms");
    		Set<String> keySet = config.keySet();
    		Iterator<String> iterator = keySet.iterator();
    		while(iterator.hasNext()){
    			String next = iterator.next();
    			String keyValue;
				try {
					keyValue = new String(config.getString(next).getBytes("ISO-8859-1"), "utf-8");
					if(keyValue.equals("true") || keyValue.equals("false")){
	    				APPCONFIG.put(next, Boolean.valueOf(keyValue));
	    				continue;
	    			}
	    			Pattern r1 = Pattern.compile("\\d+");	
	    			Matcher m1 = r1.matcher(keyValue);
	    			if(m1.matches()){
	    				APPCONFIG.put(next, Integer.valueOf(keyValue));
	    				continue;
	    			} 
	    			Pattern r2 = Pattern.compile("\\d+\\.\\d");	
	    			Matcher m2 = r2.matcher(keyValue);
	    			if(m2.matches()){
	    				APPCONFIG.put(next, Float.valueOf(keyValue));
	    				continue;
	    			} 
	    			if(keyValue.equals("null")){
	    				APPCONFIG.put(next, null);
	    				continue;
	    			}
	    			APPCONFIG.put(next, keyValue);	
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}    					
    		}
    	}
    }
    
    @Override
   	public void setApplicationContext(ApplicationContext ctx)throws BeansException {
       	factory=(SqlSessionFactory) ctx.getBean("sqlSessionFactory");	
       	transactionManager= (DataSourceTransactionManager)ctx.getBean("transactionManager");	
   		Configuration configuration = factory.getConfiguration();
   		try {
   			configuration.addMapper(Class.forName("cn.lcfms.bin.BaseDao"));
   		} catch (ClassNotFoundException e) {
   			e.printStackTrace();
   		}	
    }
    
    public static DataSourceTransactionManager getManager() {
    	return transactionManager;
    }

	public static SqlSession getSession(boolean b){		
		SqlSession session = factory.openSession(b);			
		return session;
	}
	
	public static BaseService getService(String table){			
		BaseService service=new BaseService(table);	
		return service;
	}
	
	public static BaseService getService(){		
		BaseService service=new BaseService();	
		return service;	
	}
      
    public static void printEnd(HttpServletRequest request){
		Userinfo.putUserInfo("TIMEEND", new Date().getTime(), request);
		Userinfo.putUserInfo("MEMORYEND", Runtime.getRuntime().freeMemory(), request);
		Userinfo.addUserInfo("log", "系统执行结束，完成时间："+((long)Userinfo.getUserInfo("TIMEEND", request)-(long)Userinfo.getUserInfo("TIMESTART", request))+"毫秒"+"，使用内存："+((long)Userinfo.getUserInfo("MEMORYSTART", request)-(long)Userinfo.getUserInfo("MEMORYEND", request))/1024+"kb\n\n\n", request);
    	logger.info(Userinfo.getUserInfo("log", request));  
    }
    
    /*
     * 随机生成一个BroadcastId
     */
	public static int getBroadcastId(){
		Random random=new Random();
		if(BROADCAST_ID==null){
			BROADCAST_ID=new HashMap<>();
			int[] value=new int[2];
			value[0]=random.nextInt(10000);
			value[1]=random.nextInt(10000);
			BROADCAST_ID.put(new Date().getTime(), value);
			return value[0];
		}else{
			Set<Entry<Long, int[]>> set=BROADCAST_ID.entrySet();
			Iterator<Entry<Long, int[]>> iterator=set.iterator();
			while (iterator.hasNext()) {
				Entry<Long, int[]> i=iterator.next();
				Long key=i.getKey();
				int[] val=i.getValue();
				//每一个小时换一次BroadcastId
				if((key+3600*1000)<new Date().getTime()){
					val[0]=val[1];
					val[1]=random.nextInt(10000);
				}	
				return val[0];
			}			
		}
		return 0;
	}	
	
	public static ArrayList<String> getControllerFile() {
		String ROOT_PATH = System.getProperty(ROOTNAME);
		String actionPath=ROOT_PATH+"WEB-INF"+File.separator+"classes"+File.separator+"cn"+File.separator+"lcfms"+File.separator+"app";
		ArrayList<String> filelist=new ArrayList<String>();
		getFileList(actionPath,filelist);
		return filelist;
	}
	/*
     * 递归获取文件
     */
    private static void getFileList(String strPath,ArrayList<String> filelist) {
        File dir = new File(strPath);
        File[] files = dir.listFiles(); 
        for (int i = 0; i < files.length; i++) {
            String path = files[i].getAbsolutePath();   
            getControllerFile(path+File.separator+"controller"+File.separator,filelist);
        }
    }
    
    private static void getControllerFile(String strPath,ArrayList<String> filelist){
    	 File dir = new File(strPath);
         File[] files = dir.listFiles(); 
         if (files != null) {
             for (int i = 0; i < files.length; i++) {
                 String fileName = files[i].getName();           
                 if (files[i].isDirectory()) { 
                	 getControllerFile(files[i].getAbsolutePath(),filelist); 
                 }else if (fileName.endsWith(".class")) { 
                     String strFileName = files[i].getAbsolutePath();
                     strFileName=strFileName.substring(strFileName.indexOf("cn"+File.separator+"lcfms"), strFileName.length()-6);                 
                     strFileName=strFileName.replace(File.separator, ".");
                     filelist.add(strFileName);
                 } else {
                     continue;
                 }
             }
         }
    }
}
