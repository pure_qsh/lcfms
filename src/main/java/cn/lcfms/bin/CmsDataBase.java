package cn.lcfms.bin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmsDataBase {	
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected int id;
	protected BaseService service;
	public CmsDataBase(HttpServletRequest request,HttpServletResponse response,int id,BaseService service) {
		this.request=request;
		this.response=response;
		this.id=id;
		this.service=service;
	}
}
