package cn.lcfms.app.admin.service;


import java.util.HashMap;
import java.util.List;
import org.springframework.stereotype.Service;
import cn.lcfms.app.admin.bean.ProcessBean;
import cn.lcfms.bin.App;
import cn.lcfms.bin.BaseService;
import cn.lcfms.utils.TimeUtils;
import cn.lcfms.utils.TimeUtils.Formatter;

@Service
public class ProcessService{
	
	/**
	 * 新增流程
	 * @param table 流程关联的表名
	 * @param id 流程关联表的id
	 * @param aid 用户id
	 * @param txt 流程描述
	 * @param status 流程状态
	 */
	public void createProcess(String table,int id,int aid,String txt,String remark,int status) {
		BaseService service = App.getService("process");
		ProcessBean process=new ProcessBean();
		process.setId(id);
		process.setAid(aid);
		process.setTableName(table);
		process.setTxt(txt);
		process.setRemark(remark);
		process.setStatus(status);
		process.setCreateDate(TimeUtils.getCurrentDateTime(Formatter.LONG));
		service.insert(process);
	}
	/**
	 * 更新流程
	 * @param table 流程关联的表名
	 * @param id 流程关联表的id
	 * @param aid 用户id
	 * @param txt 流程描述
	 * @param status 流程状态
	 */
	public void updateProcess(String table,int id,int aid,String txt,String remark,int status) {
		BaseService service = App.getService(table);
		service.where(service.getPrimaryKey()+"="+id);
		service.setData(status);
		service.update("status");
		this.createProcess(table, id, aid, txt,remark,status);	
	}
	/**
	 * 返回流程日志
	 * @param table
	 * @param id
	 * @return
	 */
	public List<HashMap<String, Object>> getProcess(String table,int id){
		BaseService service = App.getService("process ps");
		service.leftJoin("admin an").on("an.aid=ps.aid");
		service.column("ps.txt","ps.remark","date_format(ps.createDate, '%Y-%m-%d') as createDate","an.realName");
		List<HashMap<String, Object>> list = service.where("ps.tableName='"+table+"' and ps.id="+id).selectList();
		return list;
	}
}
