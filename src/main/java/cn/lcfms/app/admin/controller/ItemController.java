package cn.lcfms.app.admin.controller;


import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import cn.lcfms.app.admin.bean.ItemBean;
import cn.lcfms.bin.App;
import cn.lcfms.bin.BaseCache;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;

@PermitPoll(type="系统功能",name="栏目管理",forward="/nopermit.html")
@Controller("admin.Item")
@RequestMapping("/admin/item")
public class ItemController extends AdminBaseController {
	/*
	 * 管理栏目页面
	 */
	@RequestMapping("/manage")
	public ModelAndView manage(){
		ModelAndView view=new ModelAndView("admin/frame/itemManage");	
		return view;
	}
	@RequestMapping("/getItemNameById")
	@ResponseBody
	public String getItemNameById(int itemId){
		BaseService service = App.getService("item");
		ItemBean bean=service.selectOne(ItemBean.class, itemId);
		if(null==bean.getItemName()){
			return "0";
		}
		return bean.getItemName();
	}
	
	
	@ResponseBody
	@RequestMapping("/getTree")
	public String getTree(int parentId,HttpServletRequest request){
		BaseService service = App.getService("item");
		List<HashMap<String, Object>> item = service.order("sort").selectList();
		String tree = new Tree(item, parentId).tree;	
		return tree;
	}
	
	private class Tree {
		private List<HashMap<String, Object>> item;
		private String tree = "";

		private Tree(List<HashMap<String, Object>> item, int parentId) {
			this.item = item;
			this.getTree(parentId);
		}

		private void getTree(int parentId) {
			this.tree += "<ol class=\"dd-list\" parentId=\"" + parentId + "\">";
			List<HashMap<String, Object>> list = this.item;
			for (int i = 0; i < list.size(); i++) {
				if ((int) list.get(i).get("parentId") == parentId) {
					this.tree += "<li class=\"dd-item dd2-item\" data-id=\""+ list.get(i).get("itemId")+ "\">"
							  +  "<div class=\"dd-handle dd2-handle\">"+list.get(i).get("itemId")+"</div>";						
					String url="";
					if (null!=list.get(i).get("url") && !list.get(i).get("url").equals("")) {
						url= "&nbsp;&nbsp;&nbsp;&nbsp;<a data-rel=\"tooltip\" data-placement=\"top\" title=\""+list.get(i).get("url")+"\" href=\"javascript:void(0);\" class=\"tooltip-info no-hover-underline\"><i class=\"fa bigger-120 fa-external-link\"></i></a>";
					}
					this.tree += "<div class=\"dd2-content\">"	
							+ "<span class=\"col-xs-6\">&nbsp;&nbsp;"+ list.get(i).get("itemName")					
							+ url
							+ "</span>"
							+ "<div class=\"pull-right action-buttons\">"					
							+ "<a class=\"blue tooltip-info\" href=\"additem?parentId="+ list.get(i).get("itemId")+ "\" data-rel=\"tooltip\" data-placement=\"top\" data-original-title=\"添加子栏目\"><i class=\"fa fa-plus bigger-130\"></i></a>&nbsp;"
							+ "<a class=\"blue tooltip-info\" href=\"update_view?itemId="+ list.get(i).get("itemId")+ "\" data-rel=\"tooltip\" data-placement=\"top\" data-original-title=\"编辑\"><i class=\"fa fa-edit bigger-130\"></i></a>&nbsp;"
							+ "<a class=\"red tooltip-info\" href=\"javascript:deleteItem("+ list.get(i).get("itemId")+ ");\"  data-rel=\"tooltip\" data-placement=\"top\" data-original-title=\"删除\"><i class=\"fa fa-trash bigger-130\"></i></a>&nbsp;"
							+ "</div>"
							+ "</div>";					
					this.getTree((int) list.get(i).get("itemId"));
					this.tree += "</li>";
				}
			}
			this.tree += "</ol>";
			this.tree = this.tree.replaceAll("<ol class=\"dd-list\" parentId=\"\\d+\"></ol>","");
		}
	}
	/*
	 * 删除栏目与其子栏目
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int itemId){
		BaseService service = App.getService("item");
		this.deleteall(itemId,service);
		set_item_cache();
		return "1";
	}

	/*
	 * 递归删除栏目
	 */
	private void deleteall(int id,BaseService service){
		service.deleteById(id);
		service.where("parentId=#{parentId}").setData(id);
		List<HashMap<String, Object>> list = service.selectList();
		for (int i = 0; i < list.size(); i++) {
			int itemId = (int) list.get(i).get("itemId");
			this.deleteall(itemId,service);			
		}
	}
	/*
	 * 更新栏目排序
	 */
	@RequestMapping("/sort")
	@ResponseBody
	public String sort(ItemBean item,HttpServletResponse response){
		BaseService service = App.getService("item");
		service.where("itemId="+item.getItemId());
		service.setData(item.getSort(),item.getParentId());
		service.update("sort","parentId");		
		List<HashMap<String, Object>> list = BaseCache.itemcache;
		for (int i = 0; i < list.size(); i++) {
			if ((int) list.get(i).get("itemId") == item.getItemId()) {
				return (String)list.get(i).get("itemName");
			}
		}
		set_item_cache();
		return "栏目ID" + item.getItemId() + "[尚未缓存栏目名称]";
	}
	
	/*
	 * 增加或修改栏目
	 */
	@RequestMapping("/add_update_item")
	public String add_update_item(String[] permitId,ItemBean bean){
		String permit=StringUtils.join(permitId,",");
		bean.setPermitId(permit);
		int itemId = bean.getItemId();
		BaseService service = App.getService("item");
		if (itemId == 0) {			
			service.insert(bean);
		} else {
			service.update(bean,"sort");
		}
		set_item_cache();
		return "local:manage";
	}
	
	/*
	 * 增加栏目页面
	 */
	@RequestMapping("/additem")
	public ModelAndView additem(ItemBean item){
		ModelAndView view=new ModelAndView("admin/frame/itemAdd");
		BaseService service = App.getService("group");
		List<HashMap<String, Object>> list = service.selectList();
		view.addObject("grouplist", list);
		view.addObject("item", item);
		service.setTable("item");
		service.sql("select max(sort) as sort from item where parentId="+item.getParentId());
		HashMap<String, Object> map = service.getResult().get(0);
		if(null!=map) {
			Object sort=map.get("sort");
			view.addObject("sort", sort);
		}else {
			view.addObject("sort", 1);
		}	
		return view;
	}
	
	/*
	 * 修改单个栏目
	 */
	@RequestMapping("/update_view")
	public ModelAndView update_view(int itemId){
		BaseService service = App.getService("item");
		ItemBean item = service.selectOne(ItemBean.class,itemId);
		ModelAndView view=new ModelAndView("admin/frame/itemAdd");
		service.setTable("group");
		List<HashMap<String, Object>> list = service.selectList();
		view.addObject("grouplist", list);
		view.addObject("item", item);
		String permitId = item.getPermitId();
		if(null!=permitId && !"".equals(permitId)){
			String[] split = permitId.split(",");
			view.addObject("split", split);
		}		
		return view;	
	}
}
