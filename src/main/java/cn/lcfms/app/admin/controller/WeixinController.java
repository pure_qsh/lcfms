package cn.lcfms.app.admin.controller;


import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.lcfms.bin.App;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.view.CommonEditView;

@PermitPoll(type="系统功能",name="微信API",forward="/login.html")
@Controller("admin.Weixin")
@RequestMapping("/admin/weixin")
public class WeixinController extends AdminBaseController{	
	
	@RequestMapping("/config")
	public CommonEditView test(HttpServletRequest request) {
		CommonEditView view=new CommonEditView("微信配置");
		BaseService service = App.getService("weixin");
		List<HashMap<String, Object>> list = service.selectList();
		list.forEach((HashMap<String,Object> map)->{
			String title=(String) map.get("title");
			String name=(String) map.get("name");
			String value=(String) map.get("value");
			if(name.equals("url")) {
				view.setInputForm(title, name, request.getAttribute("APP")+"admin/weixin/notify", "readonly");
			}else if(name.equals("token")){
				view.addObject("token", value);
				view.setViewForm("frame/token.jsp");
			}else {
				view.setInputForm(title, name, value, "");
			}			
		});
		view.setFormAction("save");
		return view;
	}	
}
