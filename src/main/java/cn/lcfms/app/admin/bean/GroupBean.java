package cn.lcfms.app.admin.bean;

public class GroupBean{	
	private int gid;
	private String gname;
	private String gdesc;
	private String gpermission;
	public int getGid() {
		return gid;
	}
	public void setGid(int gid) {
		this.gid = gid;
	}
	public String getGname() {
		return gname;
	}
	public void setGname(String gname) {
		this.gname = gname;
	}
	public String getGdesc() {
		return gdesc;
	}
	public void setGdesc(String gdesc) {
		this.gdesc = gdesc;
	}
	public String getGpermission() {
		return gpermission;
	}
	public void setGpermission(String gpermission) {
		this.gpermission = gpermission;
	}
	

}
