function openNewTab(url,title,itemId){
	if(top.location!=self.location){
		if(!itemId){
			itemId=randomNumber(10000,99999);
		}
		parent.openNewTab(itemId,url,title);
		
	}else{
		window.location.href = url;  
	}		
}

function randomNumber(min,max){
	return Math.floor(min+Math.random()*(max-min));
}