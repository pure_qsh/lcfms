<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,minimum-scale=1,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="" />
<meta name="keywords" content="" />
<script type="text/javascript" src="${JS}jquery.min.js"></script>
<script type="text/javascript" src="${JS}swiper.min.js"></script>
<script type="text/javascript" src="${JS}lCalendar.min.js"></script>
<link rel="stylesheet" href="${CSS}swiper.min.css"/>
<link rel="stylesheet" href="${CSS}lCalendar.css" />
<link rel="stylesheet" href="${CSS}common.css">
<link rel="stylesheet" href="${CSS}style.css">
<base target="_blank" /> 
<title>贵遵护理院肿瘤科在线咨询平台</title>
<meta name="keywords" content="中医治愈癌症,中医,癌症治疗"/>
<meta name="description" content="贵遵护理医院，拒绝放化疗治癌症，贵遵密宗魔疗法，纯中药，不放疗，不化疗，疗程短，轻痛苦，可治愈除肝癌，血癌，脑癌，胰腺癌之外的其他各类早、中、晚期癌症。欢迎来院咨询考查，电话：0851-28829120"/>
</head>
<script>
var deviceWidth = document.documentElement.clientWidth;
if(deviceWidth > 640) deviceWidth = 640;
document.documentElement.style.fontSize = deviceWidth / 6.4 + 'px';
</script>
<body style="margin:0 auto;">
<div class="head_top" id="head_top">
	<a href="tel:085128829120"><img src="${IMG}header_top.jpg" alt="top"/></a>
</div>
<header>
	<img src="${IMG}logo.png" width="100%" />
</header>
<script>
$(function(){
	var t1 = window.setTimeout(function(){
		$("header").css("marginTop",$("#head_top").height()+1+"px");
	},1000); 
})
window.onload=function(){
	$("header").css("marginTop",$("#head_top").height()+1+"px");
}
</script>
<!--导航-->
<nav>
	<a href="/" target="_self" class="border_bottom">网站首页</a>
	<a href="/mobile/index/article/12" target="_self" class="border_bottom">医院介绍</a>
	<a href="/mobile/index/article/14" target="_self" class="border_bottom">专家介绍</a>
	<a href="/mobile/index/article/13" target="_self" class="border_bottom">特效疗法</a>
	<a href="/mobile/index/category/19" target="_self">康复案例</a>
	<a href="tel:0851-28829120">电话预约</a>
	<a href="javascript:shangqiao();" target="_blank">预约挂号</a>
	<a href="/mobile/index/baiduditu">来院路线</a>
</nav>
<jsp:include page="${TEMPLATE}"/>	
<div class="main4">
   	<div class="common_nav">
   		<a href="javascript:void(0);"><img src="${IMG}main6_nav.jpg" width="100%"/></a>
   	</div>
   	<div class="main5_cont">
   		<a href="javascript:void(0);" target="_blank">
   			<img src="${IMG}main5_01.png"/>
   			<p>保护隐私</p>
   		</a>
   		<a href="javascript:void(0);" target="_blank">
   			<img src="${IMG}main5_02.png"/>
   			<p>无假日医院</p>
   		</a>
   		<a href="javascript:void(0);" target="_blank">
   			<img src="${IMG}main5_03.png"/>
   			<p>营养调理</p>
   		</a>
   		<a href="javascript:void(0);" target="_blank">
   			<img src="${IMG}main5_04.png"/>
   			<p>管家式医疗服务</p>
   		</a>
   		<a href="javascript:void(0);" target="_blank">
   			<img src="${IMG}main5_05.png"/>
   			<p>心理治疗</p>
   		</a>
   		<a href="javascript:void(0)" target="_blank">
   			<img src="${IMG}main5_06.png"/>
   			<p>免费在线咨询</p>
   		</a>
   	</div>
</div>
<!---预约挂号--start-->
<!--ty-->
<div class="w" style="width:94%;margin:0 auto 0 auto;">
	<div class="tel_ty">
		<div class="in_tt"><img src="${IMG}tel_ty.jpg"></div>		
		<div class="input">
			<input type="text" class="tel" id="callbtn_n43" name="tel" value="请输入您的电话号码，30秒之内回电" onClick="this.value = ''" onBlur="if(value == ''){value='请输入您的电话号码，30秒之内回电'}else if(value=='请输入您的电话号码，30秒之内回电'){value=''}">
			<input type="button" id="callbtn_n4_s3" class="callbtn_n" value="免费回电">
			<div class="clear"></div>
		</div>		
		<a href="javascript:shangqiao();"><img src="${IMG}i1.png">在线咨询</a>
		<a href="tel:085128829120" class="a2"><img src="${IMG}i2.png">电话咨询</a>
		<a href="javascript:shangqiao();" class="a3"><img src="${IMG}i3.png">预约挂号</a>	
	</div>
	<div class="clear_l"></div> 
	<script>
	document.getElementById("callbtn_n4_s3").onclick = function () {
		lxb.call(document.getElementById("callbtn_n43"));
	}
	</script>
</div>
<!--/ty-->
<!-- main6 -->
    <div class="main6">
    	<div class="common_nav" style="margin-top:5px;">
    		<a href="javascript:void(0);"><img src="${IMG}main7_nav.jpg" width="100%"/></a>
    	</div>
    	<div class="main6_cont">
			<script type="text/javascript">
				//表单验证
				function check(){
					var name = $('#name').val();
					var tel = $('#telphone').val();
					var time = $('#date').val();
					var bqms = $('#bqms').val();
					if(name == "")
					{
						alert("姓名不能为空");
						$('#name').focus();
						return false;
					}
					if(tel == "")
					{
						alert("联系电话不能为空");
						$('#telphone').focus();
						return false;
					}
					var tomatch=/^1[3|4|5|8][0-9]\d{4,8}$/;
					var aourtelephone=$('#telphone').val();
					if(!tomatch.test(aourtelephone))
					{
						alert("手机格式不正确");
						return false;
					}
					if(time==''){
						alert("请输入预约时间！");
						$('#date').focus();
						return false;
					}
				}
			</script>
			<!-- 表单-->
			<div class="maplist" style="font-size:0.24rem;">
				<br>
				<form action="http://guahao.lcfms.cn/plus/diy.php" enctype="multipart/form-data"  method="post">
					<input type="hidden" name="action" value="post">
					<input type="hidden" name="diyid" value="1">
					<input type="hidden" name="do" value="2">
					<input type='text' name='user_form' id='user_form' style="display:none;" value='' />
					<table style="width:97%;" cellpadding="0" cellspacing="1">
						<tbody><tr>
							<td align="right" valign="top"><div style="width: 1.2rem;">您的姓名：</div></td>
							<td style="padding-bottom:0.1rem;"><input placeholder="请输入您的姓名" type="text" name="user_name" id="name" style="width:4.2rem" class="intxt" value="">
							</td>
						</tr>
						<tr>
							<td align="right" valign="top"><div style="width: 1.2rem;">您的电话：</div></td>
							<td style="padding-bottom:0.1rem;"><input placeholder="请输入您的电话" type="text" name="user_tel" id="telphone" style="width:4.2rem" class="intxt" value="">
							</td>
						</tr>
						<tr>
							<td align="right" valign="top"><div style="width: 1.2rem;">就诊时间：</div></td>
							<td style="padding-bottom:0.1rem;"><input placeholder="请输入您的就诊时间" type="text" name="user_time" id="date" style="width:4.2rem" class="intxt" value="">
							</td>
						</tr>
						<tr>
							<td align="right" valign="top"><div style="width: 1.2rem;">病情简述：</div></td>
							<td><textarea placeholder="请描述您的基本情况，如年龄、疾病类型" name="user_con" id="bqdescript" style="width:98%;height:80px"></textarea>
							</td>
						</tr>
						<input type="hidden" name="dede_fields" value="user_name,textchar;user_tel,textchar;user_time,textchar;user_disease,textchar;user_con,text;user_sex,radio;user_age,text;user_form,text;user_expert,textchar" />
						<input type="hidden" name="dede_fieldshash" value="df5b6bb804679d2cdf1ae0b4d9ae5179" />
						</tbody></table>
					<div align="center" style="height:0.8rem;padding-top:5px;">
						<input style="color:#fff;width:32%;padding: 0.2rem;background:#207140;" onclick="return check()" type="submit" name="submit" value="提 交" class="coolbg">
						<input style="color:#fff;width:32%;padding: 0.2rem;background:#207140;" type="reset" name="reset" value="重 置" class="coolbg">
					</div>
				</form>
			</div>
    	</div>
    </div>
<!---预约挂号--end-->
<!---尾部--start-->
<!-- footer -->
<footer>
    <div class="footer_map">
        <div class="footer_opera">
            <a href="/laiyuanluxian/"></a><a href="/kangaimingyuan/10.html"></a>
        </div>
    </div>
    <div class="footer_cont">
        <p>版权所有：贵遵护理医院<br />黔ICP备13005906号-1<br /><br /></p>
    </div>
    <!-- 底部导航-->
    <div id="footer_b">
        <a href="/index.html" target="_self" class="fo_dj1"></a>
        <a href="javascript:void(0)"  onclick="shangqiao();" target="_blank" class="fo_dj2"></a>
        <a href="tel:085128829120" target="_blank" class="beijing"></a>
        <a href="javascript:void(0)"  onclick="shangqiao();" class="fo_dj3"></a>
        <a href="/mobile/index/baiduditu" target="_self" class="fo_dj4"></a>
    </div>
</footer>
<script type="text/javascript">
	var headTop=$("#head_top").height()+1;
	$("header").css({"margin-top":headTop+"px"});
	//百度商桥
	var _hmt = _hmt || [];
	(function() {
	  var hm = document.createElement("script");
	  hm.src = "https://hm.baidu.com/hm.js?b95d89f9346d7ecd90507884f48db2aa";
	  var s = document.getElementsByTagName("script")[0]; 
	  s.parentNode.insertBefore(hm, s);
	})();
	function shangqiao(){
		 if ($('#nb_invite_ok').length >0){  
	         $('#nb_invite_ok').click();  
	     }  
	}	
</script>
</body>
</html>