<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>		
<!DOCTYPE html>	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${CSS}bootstrap.min.css">
		<link rel="stylesheet" href="${CSS}font-awesome.css">
		<link rel="stylesheet" href="${CSS}form.css">
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}form.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="form-box form-color-grey">
					<div class="form-header">
						<h6 class="form-title">
							<i class="fa fa-sort">
							</i>
							<lc:if test="${tag.getTagId() ne null && tag.getTagId() ne 0}">修改标签</lc:if><lc:else>添加标签</lc:else>
						</h6>
					</div>
					<form class="form-horizontal" method="post" action="add" name="myform"> 
		            	<input type="hidden" value="${tag.getTagId()}" name="tagId"/> 
						<div class="form-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">
									标签名称：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${tag.getTagName()}" name="tagName" type="text" attribute="unempty"
									placeholder="请输入标签名称" error="输入有误" success="输入正确">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									标签类型：
								</label>
								<div class="col-sm-10">
									<select class="form-control" name="parentId">
										 <option value="0">请选择</option> 
						                 <c:forEach items="${tagType}" var="val">
						                 <option value="${val.tagId}" <c:if test="${tag.getParentId() eq val.tagId}">selected</c:if>>${val.tagName}</option> 
						                 </c:forEach>   
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									UI类型：
								</label>
								<div class="col-sm-10">
									<select class="form-control" name="ui">
										<option value="0">请选择</option> 
						                <option <c:if test="${tag.ui eq 'Bootstrap UI'}">selected</c:if>>Bootstrap UI</option>   
						                <option <c:if test="${tag.ui eq 'Jquery UI'}">selected</c:if>>Jquery UI</option>  
						                <option <c:if test="${tag.ui eq 'Easy UI'}">selected</c:if>>Easy UI</option>  
						                <option <c:if test="${tag.ui eq 'Layer UI'}">selected</c:if>>Layer UI</option>  
						                <option <c:if test="${tag.ui eq 'Echarts UI'}">selected</c:if>>Echarts UI</option>			                
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									属性文件：
								</label>
								<div class="col-sm-10">
									<input class="form-control"  value="${tag.getTagFile()}" name="tagFile" type="text" attribute="unempty"
									placeholder="请输入属性文件" error="输入有误" success="输入正确">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									模版文件：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${tag.getTempFile()}" name="tempFile" type="text" attribute="unempty"
									placeholder="请输入模版文件" error="输入有误" success="输入正确">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									额外文件：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${tag.getExtraFile()}" name="extraFile" type="text"
									placeholder="请输入额外文件" error="输入有误" success="输入正确">
								</div>
							</div>
						</div>
						<div class="form-footer">
							<div class="form-group">
								<label class="col-sm-2">
								</label>
								<div class="col-sm-10">
									<button type="button" id="save" class="btn btn-sm btn-grey">
										<i class="fa fa-check">
										</i>
										提交
									</button>
									<button type="reset" id="reset" class="btn btn-sm btn-grey">
										<i class="fa fa-undo">
										</i>
										重置
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script>			
		var myform=form.init("myform");	
		$("#save").click(function(){
			myform.submit();
		});
		$("#reset").click(function(){
			myform.reset();
		});
		</script>
	</body>
</html>	