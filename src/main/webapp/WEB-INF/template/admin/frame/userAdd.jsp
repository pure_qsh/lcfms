<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>	
<!DOCTYPE html>																
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="stylesheet" href="${CSS}bootstrap.min.css">
		<link rel="stylesheet" href="${CSS}font-awesome.css">
		<link rel="stylesheet" href="${CSS}form.css">
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}../../layer/layer.js"></script>
		<script src="${JS}form.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="form-box form-color-grey">
					<div class="form-header">
						<h6 class="form-title">
							<i class="fa fa-sort">
							</i>
							<lc:if test="${user.aid ne null && user.aid ne 0}">修改用户</lc:if><lc:else>添加用户</lc:else>
						</h6>
					</div>
					<form class="form-horizontal" method="post" action="add_user" name="myform"> 
			        	<input type="hidden" name="aid" value="${user.aid}"/>
						<div class="form-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">
									用户名：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${user.aname}" name="aname" type="text" attribute="unempty gt5" placeholder="请输入用户名">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									密码：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${user.password}" name="password" type="password" attribute="unempty gt5" placeholder="请输入密码">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									重复密码：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${user.password}" name="passwordrepeat" type="password" attribute="unempty gt5"
									placeholder="请再次输入密码">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									用户组：
								</label>
								<div class="col-sm-10">
									<div class="radio">
										<c:forEach var="glist" items="${list}">
											<label class="radio-inline" for="group${glist.gid}" show="${glist.gname}">
											<input type="radio" name="gid" id="group${glist.gid}" value="${glist.gid}"								
											<c:if test="${user.gid==glist.gid}">checked</c:if>								
											/>
											<span class="lbl">${glist.gdesc}</span>
											</label>
										</c:forEach>								
									</div>
								</div>
							</div>
						</div>
						<div class="form-footer">
							<div class="form-group">
								<label class="col-sm-2">
								</label>
								<div class="col-sm-10">
									<button type="button" id="save" class="btn btn-sm btn-grey">
										<i class="fa fa-check">
										</i>
										提交
									</button>
									<button type="reset" id="reset" class="btn btn-sm btn-grey">
										<i class="fa fa-undo">
										</i>
										重置
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script>		
		var myform=form.init("myform");	
		$("#save").click(function(){
			myform.submit();
		});
		$("#reset").click(function(){
			myform.reset();
		});
		</script>
	</body>
</html>				
				
				
				