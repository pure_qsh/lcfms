<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<div class="form-group">
	<label class="col-sm-2 control-label">令牌(Token)：</label>
	<div class="col-sm-8">
		<input class="form-control" name="token" type="text" value="${token}"/>						
	</div>
	<div class="col-sm-2">
	<button type="button" class="btn btn-grey">
			<i class="fa fa-check align-top bigger-125">
			</i>
			生成token
	</button>
	</div>
</div>