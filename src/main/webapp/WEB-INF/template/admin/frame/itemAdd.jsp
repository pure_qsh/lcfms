<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>	
<!DOCTYPE html>																	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${CSS}bootstrap.min.css">
		<link rel="stylesheet" href="${CSS}font-awesome.css">
		<link rel="stylesheet" href="${CSS}form.css">
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}form.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="form-box form-color-grey">
					<div class="form-header">
						<h6 class="form-title">
							<i class="fa fa-sort">
							</i>
							<lc:if test="${item.getItemId() ne null && item.getItemId() ne 0}">修改栏目</lc:if><lc:else>添加栏目</lc:else>
						</h6>
					</div>
					<form class="form-horizontal" name="myform" method="post" action="add_update_item">
		            	<input type="hidden" name="itemId" value="${item.getItemId()}"/>
		            	<input type="hidden" name="sort" value="${sort+1}"/>
						<div class="form-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">
									栏目名称：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${item.getItemName()}" name="itemName" type="text" attribute="unempty"
									placeholder="请输入栏目名称" error="输入有误" success="输入正确">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									父栏目ID：
								</label>
								<div class="col-sm-10">
									<input class="form-control" id="parentId" value="${item.getParentId()}" name="parentId" type="text" attribute="unempty +integer"
									placeholder="请输入父栏目ID" error="输入有误" success="输入正确">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									URL：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${item.getUrl()}" name="url" type="text"
									placeholder="请输入url" error="输入有误" success="输入正确">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									用户组显示：
								</label>
								<div class="col-sm-10">								
		                         	<div class="checkbox">
		                         	    <c:forEach items="${grouplist}" var="val">
										<label class="checkbox-inline">
											<input value="${val.gid}" type="checkbox" name="permitId"
											<c:forEach items="${split}" var="gsl">
												<c:if test="${gsl eq val.gid}">checked</c:if>
											</c:forEach>									
											/>
											<span class="lbl"> ${val.gdesc}</span>
										</label>
										</c:forEach>   
									</div>		
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									icon图标：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${item.icon}" name="icon" type="text" attribute="unempty"
									placeholder="请输入icon图标" error="输入有误" success="输入正确">
								</div>
							</div>
						</div>
						<div class="form-footer">
							<div class="form-group">
								<label class="col-sm-2">
								</label>
								<div class="col-sm-10">
									<button type="button" id="save" class="btn btn-sm btn-grey">
										<i class="fa fa-check">
										</i>
										提交
									</button>
									<button type="reset" id="reset" class="btn btn-sm btn-grey">
										<i class="fa fa-undo">
										</i>
										重置
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script>		
		function checkname(itemId){
			$.get("getItemNameById?itemId="+itemId,function(res){
				if(res==0){
					myform.error("parentId","不存在该栏目！");
				}else{
					myform.success("parentId",res);
				}			
			});
		}
		var myform=form.init("myform");	
		var parentId=myform.getValue("parentId");
		if(parentId==0){
			myform.success("parentId","根栏目");
		}else{
			checkname(parentId);
		}
		
		$("#parentId").blur(function(){
			var t=Number($(this).val());
			if(!isNaN(t)){
				checkname(t);
			}else{
				myform.error("parentId","请输入整数！");
			}	
		});
		
		$("#save").click(function(){
			if(myform.hasError()){
				return;
			}
			myform.submit();
		});
		
		$("#reset").click(function(){
			myform.reset();
		});
		</script>
	</body>
</html>									