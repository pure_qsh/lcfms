<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>	
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>欢迎使用老成FMS (5.0正式版)-java web框架</title>
		<meta name="keywords" content="java快速开发框架,java编程,java web"> 
	    <meta name="description" content="老成fms(Frame Management System)，是一款前端，后台，业务集成的快速开发框架">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="${APP}statics/ace/css/bootstrap.min.css" />
		<link rel="stylesheet" href="${APP}statics/ace/css/font-awesome.min.css"/>	
		<link rel="stylesheet" href="${APP}statics/ace/css/b.tabs.css" />		
		<link rel="stylesheet" href="${APP}statics/ace/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="${APP}statics/ace/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="${APP}statics/ace/css/ace-skins.min.css" />
		<link rel="stylesheet" href="${APP}statics/ace/css/ace-rtl.min.css" />
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="${APP}statics/ace/css/ace-ie.min.css" />
		<![endif]-->		
		<!--[if !IE]> -->
		<script src="${APP}statics/ace/js/jquery-2.1.4.min.js"></script>
		<!-- <![endif]-->	
		<!--[if IE]>
		<script src="${APP}statics/ace/js/jquery-1.11.3.min.js"></script>
		<![endif]-->	
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='${APP}statics/ace/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>	
		<script src="${APP}statics/ace/js/bootstrap.min.js"></script>
		<script src="${APP}statics/ace/js/b.tabs.min.js"></script>
		<script src="${APP}statics/ace/js/ace-elements.min.js"></script>
		<script src="${APP}statics/ace/js/ace.min.js"></script>
		<script src="${APP}statics/ace/js/ace-extra.min.js"></script>
		<!--[if lte IE 8]>
		<script src="${APP}statics/ace/js/html5shiv.min.js"></script>
		<script src="${APP}statics/ace/js/respond.min.js"></script>
		<![endif]-->
		<script src="${APP}statics/tag/js/alert.js"></script>
		<style>
		.alert{
			box-shadow: 0 3px 10px #ADA7A7;
			animation: animJelly 0.4s;
		}
		@keyframes animJelly { 
			0% { opacity:0; -webkit-transform: translate3d(0,calc(200% + 30px),0) scale3d(0,1,1); -webkit-animation-timing-function: ease-in; transform: translate3d(0,calc(200% + 30px),0) scale3d(0,1,1); animation-timing-function: ease-in; }
			40% { opacity:0.5; -webkit-transform: translate3d(0,0,0) scale3d(0.02,1.1,1); -webkit-animation-timing-function: ease-out; transform: translate3d(0,0,0) scale3d(0.02,1.1,1); animation-timing-function: ease-out; }
			70% { opacity:0.6; -webkit-transform: translate3d(0,-40px,0) scale3d(0.8,1.1,1); transform: translate3d(0,-40px,0) scale3d(0.8,1.1,1); }
			100% { opacity:1; -webkit-transform: translate3d(0,0,0) scale3d(1,1,1); transform: translate3d(0,0,0) scale3d(1,1,1); }
		}
		</style>
	</head>
	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="index.html" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							老成FMS (5.0正式版)
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right hidden-xs" role="navigation">					
					<ul class="nav ace-nav">
						<li class="green dropdown-modal">
							<a class="dropdown-toggle" href="${APP}index/tag/init" target="_blank">
								UI在线编辑器
							</a>
						</li>

						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="${APP}statics/ace/images/avatars/user.jpg"/>
								<span class="user-info">
									<small>欢迎你,</small>
									${USERINFO.aname}
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-cog"></i>
										系统设置
									</a>
								</li>
								<lc:permit gid="1">
								<li>
									<a href="javascript:$.get('${APP}admin/index/clearCache',function(){alert('更新成功！');})">
										<i class="ace-icon fa fa-cog"></i>
										更新缓存
									</a>
								</li>
								</lc:permit>
								<li>
									<a href="profile.html">
										<i class="ace-icon fa fa-user"></i>
										个人信息
									</a>
								</li>
								<li>
									<a href="${APP}admin/user/quit">
										<i class="ace-icon fa fa-power-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						
						</li>
					</ul>
				
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-primary" style="width: 60px;font-size:12px;" id="prebtn">
								<i class="ace-icon fa fa-arrow-left"></i> 上一步
							</button>
							<button class="btn btn-success" style="width: 50px;font-size:12px;" id="refreshbtn">
								<i class="ace-icon fa fa-refresh"></i> 刷新
							</button>
							<button class="btn btn-purple" style="width: 60px;font-size:12px;" id="nextbtn">
								<i class="ace-icon fa fa-arrow-right"></i> 下一步
							</button>
						</div>
						<script>
							$("#prebtn").click(function(){
								var active=$("#mainFrameTabs>ul>li[class*='active']");
								var href=$($(active).children("a")).attr("href");
								href=href.substr(7);
								var w=$('#iframebTabs_'+href)[0].contentWindow;
								w.history.go(-1);
							});
							$("#refreshbtn").click(function(){
								var active=$("#mainFrameTabs>ul>li[class*='active']");
								var href=$($(active).children("a")).attr("href");
								href=href.substr(7);
								var w=$('#iframebTabs_'+href)[0].contentWindow;
								w.history.go(0);
							});
							$("#nextbtn").click(function(){
								var active=$("#mainFrameTabs>ul>li[class*='active']");
								var href=$($(active).children("a")).attr("href");
								href=href.substr(7);
								var w=$('#iframebTabs_'+href)[0].contentWindow;
								w.history.go(1);
							});
						</script>
						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>
	
							<span class="btn btn-info"></span>
	
							<span class="btn btn-warning"></span>
	
							<span class="btn btn-danger"></span>
						</div>
				</div>
				<jsp:include page="left.jsp"/>
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="span10" id="mainFrameTabs">
					    <ul class="nav nav-tabs" role="tablist">
					        <li role="presentation" class="active noclose"><a href="#bTabs_186" data-toggle="tab">控制台</a></li>
					    </ul>				     
					    <div class="tab-content">
					        <div class="tab-pane active" id="bTabs_186">
					            <iframe id="iframebTabs_186"  style="width:100%;height:100%;border:0px;overflow:scroll;" src="${APP}admin/index/first"></iframe>
					        </div>
					    </div>				 
					</div>
					<script>
						    (function(){						    	
						    	$('#mainFrameTabs').bTabs({
									sortable:true,
								    'resize' : function(){							    	
								        $('#mainFrameTabs').css("margin-top","5px");
								        $('#mainFrameTabs>.tab-content').css("border","0");
								        $('#mainFrameTabs>.tab-content').css("padding","10px");
								    }							    
								});
								$('a',$('#leftItem')).on('click', function(e) {
									var m=$(this).hasClass("dropdown-toggle");
									var itemId=$(this).attr("itemId");
									var title=$(this).attr("title");
									var url=$(this).attr("url");
									if(!m){	
										$('#mainFrameTabs').bTabsAdd(itemId,title,"${APP}"+url);	
										$('#mainFrameTabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
										   showLeftCheck();
										})
										showLeftCheck();																								
									}
								});							
								showLeftCheck();
								//检测窗口高度
								setInterval(function(){								
									var active=$("#mainFrameTabs>ul>li[class*='active']");
									var href=$($(active).children("a")).attr("href");
									href=href.substr(7);								
									if(!isNaN(href)){						
										var height=$($('#iframebTabs_'+href)[0].contentWindow.document.body).height();
										if(height<500){
											height=screen.height-240;
										}else{
											height=height;				
										}	
										$('#mainFrameTabs>.tab-content').height(height);	
									}																			
								},20)
						    })()	
						    //初始化消息
							$(function(){
								var ws;
								var url="${APP}";
								var target=url.substr(7,url.length);
								target ="ws://"+target+"websocket/getSocketConnct?aname=${USERINFO.aname}&aid=${USERINFO.aid}&id=${USERINFO.broadid}";		
								if ('WebSocket' in window) {
									ws = new WebSocket(target);			
								} else if ('MozWebSocket' in window) {
									ws = new MozWebSocket(target);			
								} else {
									alert("您的浏览器不支持websocket！");
									return;
								}			
								ws.onmessage = function(event) {
									if(event.data){
										eval("var obj="+event.data);
										var type=obj.type;
										$.alert({
						    				title:'提示',
						    				content:obj.content,
						    				style:obj.style,
						    				position:'topRight',
						    				closeBtn:true,
						    				autoClose:true
						    			});
									}
								};
							});	
						    
						    function showLeftCheck(){
								var active=$("#mainFrameTabs>ul>li[class*='active']");
								var href=$($(active).children("a")).attr("href");
								href=href.substr(7);	
								if(parseInt(href)>10000)return;
								if(!isNaN(href)){
									 	$("#leftItem li").removeClass("active");
										$("#leftItem li").removeClass("open");	
										$("#leftItem ul").removeClass("nav-show");	
										$("#leftItem ul").hide();							
										$("#leftItemId_"+href).addClass("active");
										$("#leftItemId_"+href).parents("ul").addClass("nav-show");
										$("#leftItemId_"+href).parents("ul").show();
										$("#leftItemId_"+href).parents("li").addClass("open active");	
								}														
							}		
						    
						    function openNewTab(itemId,url,title){
						    	$('#mainFrameTabs').bTabsAdd(itemId,title,url);	
						    	$('#mainFrameTabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
									   showLeftCheck();
								});
								showLeftCheck();
						    }
						    	    
					</script>				
				</div>
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->	
	</body>
</html>
