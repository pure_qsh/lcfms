<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}alert.js"></script>
	<style>
		body{
			text-align:center;
			padding-top:100px;
		}
		.alert{
			box-shadow: 0 3px 10px #ADA7A7;
			animation: animJelly 0.4s;
		}
		.alert.hideAlert{
			animation: animJelly 0.4s;
		}
		@keyframes animJelly { 
			0% { opacity:0; -webkit-transform: translate3d(0,calc(200% + 30px),0) scale3d(0,1,1); -webkit-animation-timing-function: ease-in; transform: translate3d(0,calc(200% + 30px),0) scale3d(0,1,1); animation-timing-function: ease-in; }
			40% { opacity:0.5; -webkit-transform: translate3d(0,0,0) scale3d(0.02,1.1,1); -webkit-animation-timing-function: ease-out; transform: translate3d(0,0,0) scale3d(0.02,1.1,1); animation-timing-function: ease-out; }
			70% { opacity:0.6; -webkit-transform: translate3d(0,-40px,0) scale3d(0.8,1.1,1); transform: translate3d(0,-40px,0) scale3d(0.8,1.1,1); }
			100% { opacity:1; -webkit-transform: translate3d(0,0,0) scale3d(1,1,1); transform: translate3d(0,0,0) scale3d(1,1,1); }
		}
	</style>
</head>
<body>
<div class="container-fluid">
<div class="row">
	<button onclick="showAlert();" type="button" class="btn btn-default">
	  点击这里触发
	</button>
</div>
</div>
<script>
	function showAlert(){
		$.alert({
			title:'提示',
			content:"这里是提示的内容，<a href='#' class='alert-link'>超链接</a>",
			style:'alert-success',
			position:'topRight',
			closeBtn:true,
			autoClose:true
		});
	}
</script>
</body>
</html>
 