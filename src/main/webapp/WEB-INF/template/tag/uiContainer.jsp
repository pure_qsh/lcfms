<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
.slider-h{width:100%;margin-left: 10px;}
#slider-range-h{width:95%;float:left;}
table{width: 100%;}
#modal-table{position:absolute;width:100%;height:100%;}
#icon-spinner{position:absolute;}
#showSj{color: #fff;font-size: 14px;}
</style>
<table>
<tr>
<td id="showMain" valign="top">
<div class="widget-box widget-color-blue">
	<div class="widget-header widget-header-small">
		<h6 class="widget-title">
			<i class="ace-icon fa fa-sort"></i>
			界面窗口
		</h6>		
		<span class="label label-success arrowed-in-right arrowed" style="padding-top: 4px;">友情提示：打包下载可以获取到定制的JS与CSS文件支持</span>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="slider-h">
					<span class="ui-slider-green" id="slider-range-h"></span>
					<iframe id="modalConetent" style="float:left;margin-top:10px;width:95%;min-height:520px;border:1px dashed #928484;" src="${APP}index/tag/hello"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
</td>
<td style="width:15px;"></td>
<td id="showRight" valign="top" style="width:300px;">
<div class="widget-box widget-color-blue">
	<div class="widget-header widget-header-small">
		<h6 class="widget-title">
			<a href="javascript:showright();"><i class="fa fa-angle-double-right" id="showSj"></i></a>&nbsp;&nbsp;设计窗口
		</h6>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="row">
				<div class="col-xs-6 col-sm-2">							        
			        <button class="btn btn-primary" onclick="javascript:clearHtml();"><i class="icon-cut align-top bigger-125"></i>清空当前</button>
					<div class="blank view"><div class="space-10"></div></div>
					<button class="btn btn-primary" id="showYilai"><i class="icon-copy align-top bigger-125"></i>查看依赖</button>
					<div class="blank view"><div class="space-10"></div></div>
					<button class="btn btn-primary" id="showHtml"><i class="icon-copy align-top bigger-125"></i>查看html</button>
					<div class="blank view"><div class="space-10"></div></div>
					<button class="btn btn-primary" id="copyHtml"><i class="icon-copy align-top bigger-125"></i>拷贝html</button>		
					<div class="blank view"><div class="space-10"></div></div>
					<button class="btn btn-primary" onclick="javascript:download();"><i class="icon-download-alt align-top bigger-125"></i>打包下载</button>
				</div>				
				<div class="col-xs-6 col-sm-10">
				<iframe id="modalEdit" style="border:0;width:100%;min-height:535px;"></iframe>
				</div>			
			</div>
		</div>
	</div>
</div>
</td>
</tr>
</table>
<div class="modal fade" id="hidecode">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">     
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>       
        <h4 class="modal-title">查看HTML代码</h4>
      </div>
      <div class="modal-body">
          <pre style="min-height:60px;" id="showcode"></pre>
      </div>
    </div>
  </div>
</div>
<form name="formHtml" action="${APP}index/tag/download" method="post" target="_bank">
<input type="hidden" value="" name="htmlContent"/>
<input type="hidden" value="" name="tagId"/>
</form>	
<script type="text/javascript">
	jQuery(function($) {				
		$("#slider-range-h").empty().slider({
			value: 95,
			range: "min",
			animate: true,
			step:1,
			max: 95,
			slide: function( event, ui ) {
				var val = parseInt(ui.value);	
				if(val<30){
					return false;
				}
				if(! ui.handle.firstChild ) {
					$(ui.handle).append("<div class='tooltip top in' style='display:none;left:-15px;top:-35px;'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>");
				}
				$(ui.handle.firstChild).show().children().eq(1).text(val+"%");	
				$("#modalConetent").width((val-2)+"%");
			}				
		});
		$("#slider-range-h").children(".ui-slider-handle").blur(function(){
			$(this).children(".tooltip").hide();
		});
		showright();
	});
	
	var showRight=true;
	function showright(){
		var width=$("#showRight").width();
		if(showRight){
			$("#showRight").animate({width:width*0.2});
			$("#showSj").attr("class","fa fa-angle-double-left");
			$("#modalEdit").hide();
		}
		if(!showRight){
			$("#showRight").animate({width:width/0.2});
			$("#showSj").attr("class","fa fa-angle-double-right");
			$("#modalEdit").show();
		}
		showRight=!showRight;
	}
	
    var lasttemp="";
	function getTagFile(tempFile,tagFile,tagId){
		 document.formHtml.tagId.value=tagId;
		 if(tagFile!=''){
			 document.getElementById("modalEdit").src="${APP}index/tag/getTagFile?file="+tagFile;
			 if(!showRight){
	              showright();
	         }
		 }	
    	 var b=nextgetTemp(tempFile);
		 if(b){
			    var modalConetent=$(document.getElementById("modalConetent").contentWindow.document.body);
				$.ajax({
				        url: "${APP}index/tag/getTagFile",
				        dataType: 'text',
				        type:'post',
				        data:{
				        	file:tempFile
				        },
				        success: function (res) {
				        	var js_begin="<!--js_begin-->";    
				            var js_end="<!--js_end-->";    
				            res=res.substr(res.indexOf(js_begin)+15);    
				            res=res.substring(0,res.indexOf(js_end));    
				        	modalConetent.find("#appendHtml").append(res);
							afterGetCode(tempFile);				        	
				        }
			   });  
		 }else{
        	   document.getElementById("modalConetent").src="${APP}index/tag/getTagFile?file="+tempFile;	      	  
  	      	   lasttemp=tempFile;
		 } 			 
	}	
	
    function nextgetTemp(tempFile){
    	var lyrow=['lyrow-12-temp','lyrow-4x8-temp','lyrow-6x6-temp','lyrow-8x4-temp','lyrow-4x4x4-temp'];
    	if(contains(lyrow,tempFile) && contains(lyrow,lasttemp)){
    		return true;
    	}
    	var form=['conta-form-temp','box-text-temp','box-textarea-temp','box-radio-temp','box-checkbox-temp','box-select-temp'];
    	if(contains(form,tempFile) && contains(form,lasttemp)){   		
    		return true;
    	}
    	return false;
    }
    
    function contains(array, obj) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === obj) {
                return true;
            }
        }        
        return false;
    }
    
	$(document).ready(function(){
		$.get("${APP}index/tag/drag",function (res) {
	           res=res.replace(/\\${JS}/g,"${JS}");
	           res=res.replace(/\\${CSS}/g,"${CSS}");
	           res=res.replace(/\\${IMG}/g,"${IMG}");
	           $("#dragmenu").append(res);
	           $('[data-rel=tooltip]').tooltip();
	    });	
		
		$("#copyHtml").zclip({
			path: "${JS}ZeroClipboard.swf",
			copy: function(){							
				return getcode("",lasttemp);
			},
			beforeCopy:function(){/* 按住鼠标时的操作 */
				
			},
			afterCopy:function(){/* 复制成功后的操作 */
				alert("复制成功！");
	        }
		});
		
		$("#showHtml").click(function(){
			var code=getcode("",lasttemp);
			$("#showcode").html("<code>"+html_encode(code)+"</code>");
			$('#hidecode').modal('toggle');
		});
		
		$("#showYilai").click(function(){
			var code=getcode("head",lasttemp);
			$("#showcode").html("<code>"+html_encode(code)+"</code>");
			$('#hidecode').modal('toggle');
		});
	});

	function download(){		
		var html=getcode("all",lasttemp);
		document.formHtml.htmlContent.value=html;
		if(document.formHtml.tagId.value==""){
			alert("先设计好你的页面再下载！");
			return;
		}
		document.formHtml.submit();
	}
	
	function clearHtml(){
		lasttemp="";
		document.getElementById("modalConetent").src="";
		$("#showcode").html("");		
	}
</script>