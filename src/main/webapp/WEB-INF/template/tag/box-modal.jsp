<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">窗口：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="win" type="radio"  class="ace" value="1"/> 
			<span class="lbl"> 小窗口</span>
		</label>
		<label>
			<input name="win" type="radio"  class="ace" value="2" checked/> 
			<span class="lbl"> 普通窗口</span>
		</label>
		<label>
			<input name="win" type="radio"  class="ace" value="3"/> 
			<span class="lbl"> 大窗口</span>
		</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  	
	function saveEdit(){
		var modalElement=getEditHtml();
		var dialog=modalElement.children().find(".modal-dialog");
		dialog.removeClass("modal-sm");
		dialog.removeClass("modal-lg");
		var win=$("input[name=win]:checked").val();	
		if(win==1){
			dialog.addClass("modal-sm");
		}else if(win==3){
			dialog.addClass("modal-lg");
		}
	}	
</script>
</body>
</html>

