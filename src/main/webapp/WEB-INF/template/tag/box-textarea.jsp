<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
	<style>
		overflow-y: visible;
	</style>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">标题：</label>
	<div class="controls">
		<input placeholder="标题" type="text" name="title" value="多行文字"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">长高：</label>
	<div class="controls">
		cols-<input type="text" name="cols" placeholder="50" value="50" style="width:33%"/> rows-<input type="text" name="rows" placeholder="5" value="5" style="width:33%"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">最大字数：</label>
	<div class="controls">
		<div class="clearfix">
			<input type="text" name="maxnum" placeholder="字数" value="50"/>
		</div>		
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">name：</label>
	<div class="controls">
		<div class="clearfix">
			<input type="text" name="formname" placeholder="name" value="laocheng"/>
		</div>		
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  
		function saveEdit(){	
			var modalElement=getEditHtml();
			var formname=document.Editform.formname.value;
			modalElement.children().find("textarea:last").attr("name",formname);	
			var cols=document.Editform.cols.value;
			var rows=document.Editform.rows.value;
			modalElement.children().find("textarea:last").attr("cols",cols);
			modalElement.children().find("textarea:last").attr("rows",rows);
			var maxnum=document.Editform.maxnum.value;
			modalElement.children().find("textarea:last").attr("maxnum",maxnum);
			var title=document.Editform.title.value;
			modalElement.children().find(".control-label:last").html(title+"：");	
			getEditWindow().form.init();	
		}	
</script>
</body>
</html>
