<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<link href="${CSS}jquery-ui.custom.min.css" rel="stylesheet" type="text/css"/>
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">样式：</label>
	<div class="controls">
		<select class="form-control input-medium" name="style">
			<option value="1">横向上方</option>
			<option value="2">竖向左方</option>
			<option value="3">竖向右方</option>
			<option value="4">横向下方</option>			
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">选项卡背景：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="checkbox" type="radio"  class="ace" value="1"/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="checkbox" type="radio"  class="ace" value="0" checked/>
			<span class="lbl"> 否</span>
		</label>
		</div>
		（仅选项卡横向时可以使用）
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">内容边框：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="borders" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="borders" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div id="tabnumber">
	<div class="form-group tableline">
		<label class="control-label" type="text">第1列：</label>
		<div class="controls">
			列名-<input type="text" placeholder="栏目1" class="linename" style="width:33%" value="栏目1"/> 
		</div>
	</div>
	<div class="form-group tableline">
		<label class="control-label" type="text">第2列：</label>
		<div class="controls">
			列名-<input type="text" placeholder="栏目2" class="linename" style="width:33%" value="栏目2"/>
			<a class="red" href="#" onclick="deleteline(this);">
				<i class="icon-trash bigger-130"></i>
			</a>
		</div>
	</div>
	<div class="form-group tableline">
		<label class="control-label" type="text">第3列：</label>
		<div class="controls">
			列名-<input type="text" placeholder="栏目3" class="linename" style="width:33%" value="栏目3"/>
			<a class="red" href="#" onclick="deleteline(this);">
				<i class="icon-trash bigger-130"></i>
			</a>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		<button class="btn btn-xs btn-success addline"><i class="icon-plus"></i>增加栏目</button>
	</div>
</div>
<div id="hidetableline" style="display:none;">
	<div class="form-group tableline">
		<label class="control-label" type="text"></label>
		<div class="controls">
			列名-<input type="text" placeholder="新增栏目" class="linename" style="width:33%" value="新增栏目"/>
			<a class="red" href="#" onclick="deleteline(this);">
				<i class="icon-trash bigger-130"></i>
			</a>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">         
	function deleteline(Element){
		$(Element).parent().parent().remove();
	    countline();	
	}
	jQuery(function($) {										
		$(".addline").click(function(){
		   var hidetableline=$("#hidetableline").html();
		   $("#tabnumber").append(hidetableline);
		   countline();	   
		});		
	});
	var linename=$("#tabnumber .tableline .linename");
	
	function saveEdit(){
		var modalElement=getEditHtml();
		var style=document.Editform.style.value;
		var classdata=modalElement.children().find(".tabbable");
		var uldata=modalElement.children().find("ul");
		var tabdata=modalElement.children().find(".tab-content");
		uldata.children().remove();
		tabdata.children().remove();
		var linename=$("#tabnumber .tableline .linename");			
			$(linename).each(function(i){
				var name=$(this).val();
				var ran=randomNumber(1000,9999);	
				var active="";
				if(i==0){
				   active=" class=\"active\"";
				}
				var str="<li"+active+"><a data-toggle=\"tab\" href=\"#panel-"+ran+"\">"+name+"</a></li>";
				uldata.append(str);
				if(i==0){
				   active=" active in";
				}
				str="<div id=\"panel-"+ran+"\" class=\"tab-pane column"+active+"\">栏目"+(i+1)+"内容</div>";
				tabdata.append(str);	
			});
			if(style==1){
				uldata.after(tabdata);
				editclass(classdata,"",["tabs-left","tabs-right","tabs-below"]);
				setBackground(uldata);
			}else if(style==2){
				uldata.after(tabdata);
				editclass(classdata,"tabs-left",["tabs-right","tabs-below"]);
			}else if(style==3){
				uldata.after(tabdata);
				editclass(classdata,"tabs-right",["tabs-left","tabs-below"]);
			}else if(style==4){
				tabdata.after(uldata);
				editclass(classdata,"tabs-below",["tabs-left","tabs-right"]);
				setBackground(uldata);
			}		
			var borders=$("input[name=borders]:checked").val();	
            if(borders==0){
			   tabdata.css("border","0");
			   tabdata.css("padding-left","0px");
			   tabdata.css("padding-right","0px");
			}else{
			   tabdata.removeAttr("style");
			}	
	}
	function handletabIds(){
		var t = randomNumber(1000,9999);	
		var n = "tabs-" + t;	
		e.attr("id", n);	
		e.find(".tab-pane").each(function(e, t) {
			var n = $(t).attr("id");
			var r = "panel-" + randomNumber(1000,9999);
			$(t).attr("id", r);
			$(t).parent().parent().find("a[href=#" + n + "]").attr("href", "#" + r)
		});
	}
	function setBackground(uldata){
		var checkbox=$("input[name=checkbox]:checked").val();
		if(checkbox==1){
			uldata.addClass("padding-12").addClass("tab-color-blue").addClass("background-blue");
		}else{
			uldata.removeClass("padding-12").removeClass("tab-color-blue").removeClass("background-blue");
		}
	}
    function editclass(Element,add,minus){
	    for(var i=0;i<minus.length;i++){
		    Element.removeClass(minus[i]);
		}
		if(add!=""){
			 Element.addClass(add);
		}
	}  	
	function countline(){
		var len=$("#tabnumber").find(".tableline");
		for(var i=0;i<len.length;i++){
			$(len[i]).find("label").html("第"+(i+1)+"列：");
			$(len[i]).find("input").attr("placeholder","栏目"+(i+1));
			$(len[i]).find("input").attr("value","栏目"+(i+1));		
		}
	}
</script>
</body>
</html>

