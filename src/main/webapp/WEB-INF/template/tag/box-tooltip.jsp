<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">提示内容：</label>
	<div class="controls">
		<input type="text" name="message" value="这里是提示内容"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">位置：</label>
	<div class="controls">
		<select class="form-control input-medium" name="position">
			<option value="top">上方</option>
			<option value="bottom">下方</option>	
			<option value="left">左方</option>
			<option value="right">右方</option>					
		</select>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">
	function saveEdit(){	
	    var modalElement=getEditHtml();
		var message=document.Editform.message.value;
		var position=document.Editform.position.value;
		var str="<span data-toggle=\"tooltip\" data-placement=\""+position+"\" title=\""+message+"\">鼠标放上去</span>"
		modalElement.children().find(".row").html(str);
		getEditWindow().$('[data-toggle=tooltip]').tooltip();
	}	
</script>
</body>
</html>
